import { Component, OnInit, Injector} from '@angular/core';

import { AppComponentBase } from '@shared/component-base/app-component-base';

@Component({
  selector: 'app-tes',
  templateUrl: './tes.html',
  styles: [],
})
export class TesComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
  }

}
